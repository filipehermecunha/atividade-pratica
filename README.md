# Questões Teóricas


## 1. O que é Git?

Git é um Sistema de Controle de Versões Distribuído. O git permite que todas as mudanças realizadas sobre o código fonte de um software sejam rastreáveis e armazenadas sem sobrescrever umas às outras. 

Precisamos manter o histórico dos nossos arquivos e das nossas modificações, pois muitas vezes mudamos arquivos em grupo, fazemos um commit. Desta forma, é possível recuperar versões anteriores do código e comparar as mudanças, para encontrar bugs e estudar otimizações.


## 2. O que é a staging area?

A Staging Area é um local temporário onde são armazenadas todas as alterações que serão adicionadas no próximo commit.

## 3. O que é o working directory?

O working directory (workspace) é essencialmente a pasta do seu projeto. São todos os arquivos que estão sendo trabalhados no momento.

## 4. O que é um commit?

São objetos que guardam o snapshot das modificações adicionadas na staging area.
Dentro de um commit são guardadas trees e blobs, que por sua vez identificam o estado dos arquivos e pastas no momento em que o commit é criado, assim como metadados como quando ele foi criado e por quem. 

## 5. O que é uma branch?

É uma ramificação do código, apenas um ponteiro que aponta para um commit e tudo anterior a esse commit. Uma branch funciona como um controle de origem que permite separar a evolução da programação em diferentes subdivisões.

## 6. O que é o head no Git?

O HEAD é um recurso que serve para sinalizar o trabalho atual, ou seja, o branch que estamos programando algo. Basicamente é um ponteiro que aponta para algum commit ou alguma branch.

## 7. O que é um merge?

É a ação de juntar os commits de duas branches.

## 8. Explique os 4 estados de um arquivo no Git.

untracked – Arquivos que não estavam no último commit;
unmodified – Arquivos não modificados desde o último commit;
modified – Arquivos modificados desde o último commit; 
staged – Arquivos preparados para comitar.

## 9. Explique o comando git init.

O git init é o ponto de partida para o controle de versão em um projeto Git, ao executar o comando, um novo repositório Git vazio é criado no diretório atual.
Após sua utilização, a ferramenta passa a monitorar o estado dos arquivos no projeto.

## 10. Explique o comando git add.

É utilizado para adicionar arquivos ao pacote de alterações a serem feitas.
Uma vez que um arquivo é adicionado ao pacote de alterações com o comando git add, ele está pronto para entrar no próximo commit.

## 11. Explique o comando git status.

É utilizado para verificar o status de um repositório git, bem como o estado da staging area. Ele permite que você veja quais alterações foram preparadas, quais arquivos estão sendo monitorados pelo Git e em qual branch você está no projeto.


## 12. Explique o comando git commit.

É utilizado para criar uma nova versão do projeto a partir de um pacote de alterações. Ele leva as mudanças de um ambiente local para o repositório no git. Para cada commit é necessário escrever uma mensagem para identificá-lo, com uma mensagem clara de quais alterações foram feitas neste commit.

## 13. Explique o comando git log.

É utilizado para ver o histórico de alterações do projeto, onde aparecerão todos os commits feitos, com suas respectivas mensagens e códigos identificadores. O comando é muito útil quando precisamos rastrear o andamento de um projeto e verificar em qual ponto cada funcionalidade foi implementada. 

## 14. Explique o comando git checkout -b.

Este comando cria uma nova branch e muda para ela após sua criação.

## 15. Explique o comando git reset e suas três opções.

O comando git reset serve para desfazer alterações realizadas no git.
### git reset --soft:
O comando git reset --soft, move o HEAD para o commit indicado, mas mantém o staging e o working directory inalterados.

### git reset --mixed
O comando git reset --mixed, move o HEAD para o commit indicado, altera o staging e mantém o working directory.

### git reset --hard
O comando git reset --hard faz com que o HEAD aponte para algum commit anterior, mas também altera a staging area e o working directory para o estado do commit indicado, ou seja, todas as alterações realizadas após o commit ao qual retornamos serão perdidas. Isso não é recomendável se as alterações já tiverem sido enviadas para o repositório remoto. Nesse caso devemos utilizar o git revert.

## 16. Explique o comando git revert.
O comando git revert é utilizado para registrar alguns commits novos para reverter o  que foi feito dentro de um determinado commit (ou dentro de um intervalo de commits).

## 17. Explique o comando git clone.

É utilizado para criar uma cópia de um repositório remoto em um diretório da máquina. Este repositório pode ser criado a partir de um repositório armazenado localmente, através do caminho absoluto ou relativo, ou pode ser remoto, através do URI na rede.
A partir de um repositório clonado, é possível acompanhar o estado de um projeto e suas modificações, além de contribuir com o projeto, a partir do envio das suas modificações ao repositório central.

## 18. Explique o comando git push

O comando git push é usado para fazer upload do conteúdo do repositório local para um repositório remoto. Pushing é como você transfere commits do seu repositório local para um repositório remoto.

## 19. Explique o comando git pull.

O comando git pull serve para recuperar e baixar o conteúdo de um repositório remoto e atualizar o repositório local assim que ele for baixado. Ou seja, ele  traz as alterações de um repositório remoto para o local. Isso é muito útil em fluxos de trabalho de colaboração que precisam mesclar alterações upstream remotas no repositório local.

## 20. Como ignorar o versionamento de arquivos no Git?

O arquivo .gitignore é um arquivo de texto que diz ao Git quais arquivos ou pastas ele deve ignorar em um projeto. Um arquivo .gitignore geralmente é colocado no diretório raiz de um projeto.

## 21. No terralab utilizamos as branches master ou main, develop e staging. Explique o objetivo de cada uma.

### Master ou main:

É a branch principal, aqui é onde temos todo o código de produção. Todas as novas funcionalidades que forem sendo desenvolvidas, em algum momento, serão mescladas ou associadas a Main.

### Develop:

Esta branch contém o código de pré-produção. Quando as features são concluídas, elas são mescladas na branch develop. É aqui que cada nova funcionalidade é acrescentada ao longo de todo processo de desenvolvimento para posteriormente serem associadas a Main.

### Staging:

Esse branch contém uma versão do produto em processo de homologação. Versões candidatas (release candidates) do código que estão sendo testadas para que possam, então, ser integradas a main. Qualquer alteração feita neste branch deve também ser incorporada na branch Develop para propagar as correções realizadas. Por convenção, esse branch é chamado staging/* . * .* onde os asteriscos representam a versão do produto sendo preparada para lançamento seguindo as lógicas de versionamento utilizadas pelo time. 

Nessa branch é feito o processo de criar uma cópia de um branch de desenvolvimento para testes e aprovação antes de ser mesclada no branch principal. Isso ajuda a garantir que as alterações feitas no branch de desenvolvimento sejam estáveis e compatíveis com o código existente antes de serem lançadas para os usuários.


# Questões Práticas

##### 1. A essa altura, você já deve ter criado a sua conta do GitLab, não é? Crie um repositório público na sua conta, que vai se chamar Atividade Prática e por fim sincronize esse repositório em sua máquina local.

##### 2. Dentro do seu reposotorio, crie um arquivo chamado README.md e leia o artigo como fazer um readme.md bonitão e deixe esse README.md abaixo bem bonitão. 
##### README.md onde o trainne irá continuamente responder as perguntas em formas de commit. 

Inserção de código, exemplo de commit de feature. 

##### 3. Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:

node calculadora.js a b

~~~javascript
const args = process.argv.slice(2);
console.log(parseInt(args[0]) + parseInt(args[1]));
~~~

##### Descubra o que esse código faz através de pesquisas na internet, também descubra como executar um código em javascript e dado que o nome do nosso arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo como executar esse código em seu terminal:

##### RESPOSTA: 

Esse código recebe dois argumentos que são salvos como um array. Logo no console.log( ) são convertidos de string para números inteiros através da função parseInt( ) e é realiza a soma desses dois argumentos que foram convertidos para inteiro, um argumento da posição 0 e outro da posição 1.

Para executarmos esse código basta colocar no terminal:
~~~javascript
node calculadora.js n1 n2
~~~

Onde n1 e n2 são os respectivos números a serem somados:
~~~javascript
node calculadora.js 1 3

Saída: 4
~~~


##### 4. Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.

##### RESPOSTA:
Para adicionar apenas um arquivo ao seu histórico de commit, utiliza-se o comando "git add <caminho_do_arquivo>".

Nesse caso em específico utilizei o comando:

" git add .\calculadora.js"

##### - Que tipo de commit esse código deve ter de acordo ao conventional commit.

Deve receber o tipo feature(feat), pois a calculadora.js adiciona uma nova funcionalidade.

##### - Que tipo de commit o seu README.md deve contar de acordo ao conventional commit.

Deve receber o tipo documents(docs), pois o README.md é um arquivo de documentação, não inclui alterações no código.

##### - Por fim, faça um push desse commit.

##### 5. Copie e cole o código abaixo em sua calculadora.js:

~~~javascript
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const args = process.argv.slice(2);

soma();
~~~
Descubra o que essa mudança representa em relação ao conventional commit e faça o devido commit dessa mudança.

##### RESPOSTA: 

O conventional commit devido para essa mudança é o refactor, pois, não houve alteração no funcionamento do código, apenas foi reestruturado. 

##### 6. João entrou em seu repositório e o deixou da seguinte maneira:

~~~javascript
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
    console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;
default:
        console.log('does not support', arg[0]);
}
~~~
##### Depois disso, realizou um git add . e um commit com a mensagem: "Feature: added subtraction" faça como ele e descubra como executar o seu novo código. Nesse código, temos um pequeno erro, encontre-o e corrija para que a soma e divisão funcionem. Por fim, commit sua mudança.

##### RESPOSTA: 

Inicialmente ele não irá funcionar pois estamos passando args[0] tanto para o número da posição 0 que queremos realizar uma operação com ele e tanto para o switch.

##### *OBS: Não temos divisão neste código, temos adição e subtração.

Corrigindo o erro presente no código, ele ficaria assim:

~~~javascript
const soma = () => {
    console.log(parseInt(args[1]) + parseInt(args[2]));
};

const sub = () => {
    console.log(parseInt(args[1]) - parseInt(args[2]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;
default:
        console.log('does not support', args[0]);
}
~~~

##### 7. Por causa de joãozinho, você foi obrigado a fazer correções na sua branch principal! O produto foi pro saco e a empresa perdeu muito dinheiro porque não conseguiu fazer as suas contas, graças a isso o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.

##### Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch. 

##### 8. Agora que a sua divisão está funcionando e você garantiu que não afetou as outras funções, você está apto a fazer um merge request. Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.

##### 9. João quis se redimir dos pecados e fez uma melhoria em seu código, mas ainda assim, continuou fazendo um push na master, fez a seguinte alteração no código e fez o commit com a mensagem: "feat: add conditional evaluation".

~~~javascript
var x = args[0];
var y = args[2];
var operator = args[1];

function evaluate(param1, param2, operator) {
  return eval(param1 + operator + param2);
}

if ( console.log( evaluate(x, y, operator) ) ) {}
~~~
##### Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, e seu chefe não descobriu que tudo está comprometido, faça um revert através do seu gitlab para que o produto volte ao normal o quanto antes!

##### 10. Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem. 

##### RESPOSTA:  

Precisei adicionar a constante args = process.argv.slice(2). E assim, o código ficou funcional. No terminal, vamos passar os args separados pelo operador (50 / 10) e a função evaluate irá retornar o resultado da operação.

~~~javascript
node .\calculadora.js x operador y
~~~

Onde x e y = um número de sua escolha.

E o operador de nossa escolha, que pode ser: +, -, *, /, **, == e !=

Exemplo de execução:

~~~javascript
node .\calculadora.js 50 / 10

Saída: 5
~~~

A função eval() do javascript é uma função que permite a execução de código JavaScript dinamicamente. Tal que, o nosso input que neste caso é uma expressão matemática passada para args é convertido para código javascript. E por fim retorna para o usuário a expressão matemática resolvida.
